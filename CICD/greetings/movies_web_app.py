import os
import time

from flask import request
from flask import Flask, render_template
import mysql.connector
from mysql.connector import errorcode

application = Flask(__name__)
app = application


def get_db_creds():
    # db = os.environ.get("RDS_DB_NAME", None)
    # username = os.environ.get("RDS_USERNAME", None)
    # password = os.environ.get("RDS_PASSWORD", None)
    # hostname = os.environ.get("RDS_HOSTNAME", None)
    # return db, username, password, hostname

    db = os.environ.get("DB", None) or os.environ.get("database", None)
    username = os.environ.get("USER", None) or os.environ.get("username", None)
    password = os.environ.get("PASSWORD", None) or os.environ.get("password", None)
    hostname = os.environ.get("HOST", None) or os.environ.get("dbhost", None)
    return db, username, password, hostname


def create_table():
    # Check if table exists or not. Create and populate it only if it does not exist.
    db, username, password, hostname = get_db_creds()
    # table_ddl = 'CREATE TABLE message(id INT UNSIGNED NOT NULL AUTO_INCREMENT, greeting TEXT, PRIMARY KEY (id))'
    table_ddl = 'CREATE TABLE movies (id INT UNSIGNED NOT NULL AUTO_INCREMENT, year INT, title VARCHAR(150) NOT NULL, dte VARCHAR(150) NOT NULL, actor VARCHAR(150) NOT NULL, release_date DATE NOT NULL, rating DECIMAL(4,2), PRIMARY KEY (id))'

    cnx = ''
    try:
        cnx = mysql.connector.connect(user=username, password=password,
                                      host=hostname,
                                      database=db)
    except Exception as exp:
        print(exp)

    cur = cnx.cursor()

    try:
        cur.execute(table_ddl)
        cnx.commit()
        populate_data()
    except mysql.connector.Error as err:
        if err.errno == errorcode.ER_TABLE_EXISTS_ERROR:
            print("already exists.")
        else:
            print(err.msg)

def populate_data():

    db, username, password, hostname = get_db_creds()

    print("Inside populate_data")
    print("DB: %s" % db)
    print("Username: %s" % username)
    print("Password: %s" % password)
    print("Hostname: %s" % hostname)

    cnx = ''
    try:
        cnx = mysql.connector.connect(user=username, password=password,
                                       host=hostname,
                                       database=db)
    except Exception as exp:
        print(exp)

    cur = cnx.cursor()
    cur.execute("INSERT INTO message (greeting) values ('Hello, World!')")
    cnx.commit()
    print("Returning from populate_data")

def query_data():

    db, username, password, hostname = get_db_creds()

    print("Inside query_data")
    print("DB: %s" % db)
    print("Username: %s" % username)
    print("Password: %s" % password)
    print("Hostname: %s" % hostname)

    cnx = ''
    try:
        cnx = mysql.connector.connect(user=username, password=password,
                                      host=hostname,
                                      database=db)
    except Exception as exp:
        print(exp)

    cur = cnx.cursor()

    cur.execute("SELECT title FROM movies")
    entries = [dict(title=row[0]) for row in cur.fetchall()]
    return entries

try:
    print("---------" + time.strftime('%a %H:%M:%S'))
    print("Before create_table")
    create_table()
    print("After create_table")
except Exception as exp:
    print("Got exception %s" % exp)
    conn = None

@app.route('/add_movie', methods=['POST'])
def add_to_db():
    # print("Received request.")
    result = ''
    
    ye = request.form['year']
    t = request.form['title']
    d = request.form['director']
    a = request.form['actor']
    r = request.form['release_date']
    rat = request.form['rating']

    db, username, password, hostname = get_db_creds()

    cnx = ''
    try:
        cnx = mysql.connector.connect(user=username, password=password,
                                      host=hostname,
                                      database=db)

        # if_exists = ("SELECT * from movies WHERE title = %s")
        # add_movie = ("INSERT INTO movies ""(year, title, dte, actor, release_date, rating) ""VALUES (%s, %s, %s, %s, %s, %s)")
        # exist_data = (t, )
        # data = (int(ye), t, d, a, r, rat)

    except Exception as exp:
        print(exp)
        import MySQLdb
        cnx = MySQLdb.connect(unix_socket=hostname, user=username, passwd=password, db=db)

    try:
        cur = cnx.cursor()

        if_exists = ("SELECT * from movies WHERE title = %s")
        add_movie = ("INSERT INTO movies ""(year, title, dte, actor, release_date, rating) ""VALUES (%s, %s, %s, %s, %s, %s)")
        exist_data = (t, )
        data = (int(ye), t, d, a, r, rat)

        cur.execute(if_exists, exist_data)

        if len(cur.fetchall()) == 0:
            cur.execute(add_movie, data)
            if cur.rowcount != 0:
                result = 'Movie ' + t + ' successfully inserted'
        else:
            result = 'Movie ' + t + ' already exists'
    except Exception as e:
        result = 'Movie ' + t + ' could not be inserted - ' + str(e)
        return hello(result)

    cnx.commit()
    return hello(result)

@app.route('/update_movie', methods=['POST'])
def update_to_db():
    # print("Received request.")
    result = ''

    ye = request.form['year']
    t = request.form['title']
    d = request.form['director']
    a = request.form['actor']
    r = request.form['release_date']
    rat = request.form['rating']

    db, username, password, hostname = get_db_creds()

    cnx = ''
    try:
        cnx = mysql.connector.connect(user=username, password=password,
                                      host=hostname,
                                      database=db)

        # update_movie = ("UPDATE movies SET year=%s, dte=%s, actor=%s, release_date=%s, rating=%s WHERE title=%s")
        # data = (int(ye), d, a, r, rat, t)

    except Exception as exp:
        print(exp)
        import MySQLdb
        cnx = MySQLdb.connect(unix_socket=hostname, user=username, passwd=password, db=db)

    try:
        cur = cnx.cursor()

        update_movie = ("UPDATE movies SET year=%s, dte=%s, actor=%s, release_date=%s, rating=%s WHERE title=%s")
        data = (int(ye), d, a, r, rat, t)

        cur.execute(update_movie, data)
        if cur.rowcount != 0:
            result = 'Movie ' + t + ' successfully updated'
        else:
            result = 'Movie ' + t + ' could not be updated - Title not found' 
    except Exception as e:
        result = 'Movie ' + t + ' could not be updated - ' + str(e)
    
    cnx.commit()
    return hello(result)

@app.route('/delete_movie', methods=['POST'])
def delete_from_db():
    # print("Received request.")
    result = ''
    t = request.form['delete_title']

    db, username, password, hostname = get_db_creds()

    cnx = ''
    try:
        cnx = mysql.connector.connect(user=username, password=password,
                                      host=hostname,
                                      database=db)

        del_movie = ("DELETE FROM movies WHERE title = %s")
        data = (t,)

    except Exception as exp:
        print(exp)
        import MySQLdb
        cnx = MySQLdb.connect(unix_socket=hostname, user=username, passwd=password, db=db)

    try:
        cur = cnx.cursor()
        cur.execute(del_movie, data)
        if cur.rowcount == 0:
            result = 'Movie with ' + t + ' does not exist'
        else:
            result = 'Movie ' + t + ' successfully deleted'
    except Exception as e:
        result = 'Movie ' + t + ' could not be deleted - ' + str(e)
        return hello(result)

    cnx.commit()
    return hello(result)

@app.route('/search_movie', methods=['GET'])
def search_db():

    result = []
    resString = ''
    a = request.args.get('search_actor')
    
    db, username, password, hostname = get_db_creds()
    cnx = ''

    try:
        cnx = mysql.connector.connect(user=username, password=password,
                                      host=hostname,
                                      database=db)

        ser_movie = ("SELECT * from movies WHERE actor = %s")
        data = (a,)

    except Exception as exp:
        print(exp)
        import MySQLdb
        cnx = MySQLdb.connect(unix_socket=hostname, user=username, passwd=password, db=db)

    try:
        cur = cnx.cursor()
        cur.execute(ser_movie, data)

        searchdata = cur.fetchall()

        if len(searchdata) == 0 :
            result = 'No movies found for actor ' + a
            cnx.commit()
            return hello(result)
        else:

            for item in searchdata:
                t = str(item[2])
                y = str(item[1])
                a = str(item[4])

                gil = (t + ', ' + y + ', ' + a)
                result.append(gil)

    except Exception as e:
        result = 'Actor ' + a + ' could not be searched - ' + str(e)
        return hello(result)

    cnx.commit()
    return hello('', result)

@app.route('/highest_rating', methods=['GET', 'POST'])
def high_db():
    # print("Received request.")
    result = []
    resString = ''

    db, username, password, hostname = get_db_creds()

    cnx = ''
    try:
        cnx = mysql.connector.connect(user=username, password=password,
                                      host=hostname,
                                      database=db)

        high_rating = ("SELECT MAX(rating) AS maximum FROM movies")
        high_movie = ("SELECT * FROM movies WHERE rating=%s") 

    except Exception as exp:
        print(exp)
        import MySQLdb
        cnx = MySQLdb.connect(unix_socket=hostname, user=username, passwd=password, db=db)

    try:
        cur = cnx.cursor(buffered=True)
        cur.execute(high_rating)

        asdf = cur.fetchall()
        for i in asdf:
            data = float(i[0])

        cur.execute(high_movie, (data, ))
        
        for item in cur.fetchall():
            t = str(item[2])
            y = str(item[1])
            a = str(item[4])
            d = str(item[3])
            r = str(item[6])

            gil = (t + ', ' + y + ', ' + a + ', ' + d + ', ' + r)
            result.append(gil)

        # for item in result:
        #     resString += item + '<br />'
        
    except Exception as e:
        print(e)

    cnx.commit()
    # print(resString)
    return hello('', result)

@app.route('/lowest_rating', methods=['GET', 'POST'])
def low_db():
    # print("Received request.")
    result = []
    resString = ''

    db, username, password, hostname = get_db_creds()

    cnx = ''
    try:
        cnx = mysql.connector.connect(user=username, password=password,
                                      host=hostname,
                                      database=db)

        high_rating = ("SELECT MIN(rating) AS maximum FROM movies")
        high_movie = ("SELECT * FROM movies WHERE rating=%s") 

    except Exception as exp:
        print(exp)
        import MySQLdb
        cnx = MySQLdb.connect(unix_socket=hostname, user=username, passwd=password, db=db)

    try:
        cur = cnx.cursor(buffered=True)
        cur.execute(high_rating)

        asdf = cur.fetchall()
        for i in asdf:
            data = float(i[0])

        cur.execute(high_movie, (data, ))
        
        for item in cur.fetchall():
            t = str(item[2])
            y = str(item[1])
            a = str(item[4])
            d = str(item[3])
            r = str(item[6])

            gil = (t + ', ' + y + ', ' + a + ', ' + d + ', ' + r)
            result.append(gil)

        # for item in result:
        #     resString += item + '\n'
        
    except Exception as e:
        print(e)

    cnx.commit()
    # print(resString)
    return hello('', result)

@app.route("/")
def hello(result = '', entries = []):
    islist = False
    result_list = ''

    # print(entries)
    # print("Printing available environment variables")
    #print(os.environ)
    print('----------------------------------------------------------')
    print("Before displaying index.html")
    # entries = query_data()
    # print("Entries: %s" % entries)
    return render_template('index.html', entries=entries, message=result)     


if __name__ == "__main__":
    app.debug = True
    app.run(host='0.0.0.0')
